# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running `nixos-help`).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      (builtins.getFlake "gitlab:kofoednielsen/failover/f4078f743a6b06e74d6d21553dd5e87ca38a7abc").packages.x86_64-linux.module
    ];

  nix.settings.experimental-features = [ "flakes" ];

  services.routeFailover = {
    enable = true;
    mainInterface = "vlan101"; # fiber
    backupInterface = "enp0s20f1"; # 4g
    ips = [
      "1.1.1.1"
      "8.8.8.8"
      "9.9.9.9"
    ];
    timeout = 1;
    threshold = 3;
  };

  environment.systemPackages = with pkgs; [
    git
    vim
  ];

  networking.useNetworkd = true;
  networking.useDHCP = false;

  systemd.services.systemd-networkd.environment.SYSTEMD_LOG_LEVEL="debug";

  # FIREWALL

  networking.firewall.enable = false;
  networking.nftables.enable = true;
  networking.nftables.rulesetFile = ./firewall.nft;

  # INTERFACES
  systemd.network.wait-online.anyInterface = true;
  systemd.network = {
    enable = true;
    netdevs = {
      "vlan101" = {
        netdevConfig = {
          Kind = "vlan";
          Name = "vlan101";
        };
        vlanConfig.Id = 101;
      };
    };

    networks = {
      "wan-4g" = {
        matchConfig.Name = "enp0s20f1";
        networkConfig = {
          LinkLocalAddressing = "no";
          DHCP = "ipv4";
        };
        dhcpV4Config = {
          RouteMetric = 1002;
        };
      };
      "wan-fiber" = {
        matchConfig.Name = "enp4s0";
	networkConfig = {
	  LinkLocalAddressing = "no";
	};
        # tag vlan on this link
        vlan = [
          "vlan101"
        ];
      };
      "vlan101" = {
        matchConfig.Name = "vlan101";
        networkConfig = {
	  DHCP = "ipv4";
        };
        dhcpV4Config = {
          RouteMetric = 1001;
        };
      };
      "lan" = {
        matchConfig.Name = "enp3s0";
        address = [
            "192.168.1.1/24"
        ];
	networkConfig = {
	  LinkLocalAddressing = "no";
          DHCPServer = true;
        };
        dhcpServerConfig = {
	  EmitDNS = true;
          DNS="1.1.1.1";
        };
      };
      "mgmt" = {
        linkConfig.RequiredForOnline = "no";
        matchConfig.Name = "enp0s20f0";
        address = [
            "192.168.0.1/24"
        ];
        networkConfig = {
	  LinkLocalAddressing = "no";
          DHCPServer = true;
	};
        dhcpServerConfig = {
          PoolOffset = 10;
        };
        dhcpServerStaticLeases = [
          {
	    dhcpServerStaticLeaseConfig = {
              # caddy    
              MACAddress = "06:22:e9:52:a0:72";
              Address = "192.168.0.32";
            };
          }
          {
            dhcpServerStaticLeaseConfig = {
              # publickey    
              MACAddress = "9a:50:f9:46:17:b8";
              Address = "192.168.0.33";
            };
          }
          {
            dhcpServerStaticLeaseConfig = {
              # tunnel    
              MACAddress = "32:0f:76:fd:fe:40";
              Address = "192.168.0.38";
            };
          }
          {
            dhcpServerStaticLeaseConfig = {
              # wireguard    
              MACAddress = "66:f1:4c:1e:3c:bb";
              Address = "192.168.0.41";
            };
          }
          {
            dhcpServerStaticLeaseConfig = {
              # pyjam.as    
              MACAddress = "ba:b0:84:61:8b:d1";
              Address = "192.168.0.45";
            };
          }
          {
            dhcpServerStaticLeaseConfig = {
              # truenas    
              MACAddress = "46:56:43:b2:c8:ab";
              Address = "192.168.0.69";
            };
          }
          {
            dhcpServerStaticLeaseConfig = {
              # k8s    
              MACAddress = "96:24:3f:a3:08:e3";
              Address = "192.168.0.70";
            };
          }
          {
            dhcpServerStaticLeaseConfig = {
              # kofoednielsen    
              MACAddress = "92:31:b1:c3:f0:2d";
              Address = "192.168.0.87";
            };
          }
          {
            dhcpServerStaticLeaseConfig = {
              # jitsi
              MACAddress = "52:19:1e:2c:04:cd";
              Address = "192.168.0.39";
            };
          }
          {
            dhcpServerStaticLeaseConfig = {
              # slicerunner-cluster
              MACAddress = "e2:b4:6c:6a:76:e0";
              Address = "192.168.0.201";
            };
          }
          {
            dhcpServerStaticLeaseConfig = {
              # pass-cluster
              MACAddress = "ce:a4:77:f2:d9:bc";
              Address = "192.168.0.128";
            };
          }
          {
            dhcpServerStaticLeaseConfig = {
              # minecraft
              MACAddress = "8e:ae:e6:06:07:71";
              Address = "192.168.0.236";
            };
          }
          {
            dhcpServerStaticLeaseConfig = {
              # nicolai
              MACAddress = "8a:ba:98:6a:63:86";
              Address = "192.168.0.58";
            };
          }
          {
            dhcpServerStaticLeaseConfig = {
              # cephtescephtest
              MACAddress = "bc:24:11:87:1f:5b";
              Address = "192.168.0.48";
            };
          }
          {
            dhcpServerStaticLeaseConfig = {
              # alex
              MACAddress = "bc:24:11:d7:f0:5a";
              Address = "192.168.0.77";
            };
          }
          {
            dhcpServerStaticLeaseConfig = {
              # selvindsigt.pyjam.as
              MACAddress = "bc:24:11:92:fb:9f";
              Address = "192.168.0.239";
            };
          }
          {
            dhcpServerStaticLeaseConfig = {
              # valheim
              MACAddress = "bc:24:11:a0:bc:b2";
              Address = "192.168.0.163";
            };
          }
          {
            dhcpServerStaticLeaseConfig = {
              # jellyfin
              MACAddress = "bc:24:11:4e:68:22";
              Address = "192.168.0.205";
            };
          }
        ];
      };
    };
  };

  services.openssh = {
    enable = true;
    # require public key authentication for better security
    settings.PasswordAuthentication = false;
    settings.KbdInteractiveAuthentication = false;
    settings.PermitRootLogin = "yes";
  };

  users.users."root".openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEqQwkGub96g2LJTt2zFz2jp3U7Alqfapc25cCUk/4zD kofoednielsen@me"
  ];

  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/sda"; # or "nodev" for efi only

  boot.kernelParams = [
    "console=tty1"
    "console=ttyS1,115200"
  ];
  boot.kernel.sysctl = {
    "net.ipv4.conf.all.forwarding" = true;
  };

  system.stateVersion = "23.05"; # Did you read the comment?

}

